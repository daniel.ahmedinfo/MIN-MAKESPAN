from abc import ABCMeta, abstractmethod
from random import randint

class PrintAble(metaclass=ABCMeta):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def toString(self, data, file):
        raise NotImplementedError

class ScreenPrint(PrintAble):

    def toString(self, data, file=None):
        return """
        Borne inférieure ‘‘maximum’’ = {0}
        Borne inférieure ‘‘moyenne’’ = {1}
        Résultat LSA = {2}
        Résultat LPT = {3}
        Résultat MyAlgo = {4}
        """.format(data.getBornInfMax()[0],
                data.getBornInfMoy()[0],
                data.getLSA()[0],
                data.getLPT()[0],
                data.getMyAlgo()[0]
            )

class FilePrint(PrintAble):
    def toString(self,  data, file):
        with open(file, 'w', newline='') as writeFile:
            for index in range(0, len(data['LSA'])):
                writeFile.write("""
                Borne inférieure ‘‘maximum’’ = {0}
                Borne inférieure ‘‘moyenne’’ = {1}
                Résultat LSA = {2}
                Résultat LPT = {3}
                Résultat MyAlgo = {4}
                ========================================================================
                """.format(data['BORN_INF_MAX'][index],
                        data['BORN_INF_MOY'][index],
                        data['LSA'][index],
                        data['LPT'][index],
                        data['MyAlgo'][index]
                    ))
            writeFile.write("""
                ratio d’approximation moyen LSA = {0}
                ratio d’approximation moyen LPT = {1}
                ratio d’approximation moyen MyAlgo = {2}
            """.format(
                data['RATIOMOY']['LSA'],
                data['RATIOMOY']['LPT'],
                data['RATIOMOY']['MYALGO'],
            ))
        return """
        You can see the result in {0}
        """.format(file)
class State (metaclass=ABCMeta):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def doChange(self, context):
        raise NotImplementedError

    @abstractmethod
    def read(self, file = None):
        raise NotImplementedError

    def getData(self):
        data = {}
        try:
            data = {'m':self.m, 'n':self.n, 'data':self.data, 'k':self.k}
        except NameError:
            raise NameError

        return data

    @abstractmethod
    def toString(self, context, fileName = None):
        raise NotImplementedError

class IfState(State):

    def __init__(self):
        super().__init__()
        self.printAble = ScreenPrint()

    def read(self, file):
        print("read file with in If mode")
        data = []
        with open(file,'r') as file_r:
            read_data = file_r.readline()
            data = [int(x) for x in read_data.split(":")]
            file_r.close()
        self.m = data[0]
        self.n = data[1]
        self.data = data[2:]
        self.k = 1
        if(len(self.data) <=0  or self.data.__len__()!=self.n):
            raise EOFError

    def toString(self, context):
        context.setState(self)
        return self.printAble.toString(context)

    def doChange(self, context):
        context.setState(self)


class IcState(State):

    def __init__(self):
        super().__init__()
        self.printAble = ScreenPrint()

    def read(self, file=None):
        read_data = (input("Enter your instance m:n:duration1:...:durationN \n"))
        data = [int(x) for x in read_data.split(":")]
        if len(data)<=2:
            print("empty data")
            raise EOFError
        self.m = data[0]
        self.n = data[1]
        self.data = data[2:]
        self.k = 1
        if(len(self.data)!=self.n):
            raise EOFError

    def toString(self, context):
        if(self!=context.getState()):
            context.setState(self)
        return self.printAble.toString(context)

    def doChange(self, context):
        context.setState(self)

class IgState(State):

    def __init__(self):
        super().__init__()
        self.printAble = FilePrint()

    def read(self, file=None):
        k = int(input("Choose a number of Instance you want \n"))
        m = int(input("Choose a number of machine m \n"))
        n = int(input("Choose a number of process n \n"))
        mini = int(input("Choose a minimum duration \n"))
        maxi = int(input("Choose a maximum duration \n"))
        res=list()
        i=0
        j=0
        while(i<k):
            instance=list()
            #instance.append(m)
            #instance.append(n)
            while(j<n):
                instance.append(randint(mini,maxi))
                j=j+1

            res.append(instance)
            #print(res)
            i=i+1
            j=0

        self.m = m
        self.n = n
        if(k==1):
            self.data = res[0]
        else:
            self.data = res
        self.k = k

    def toString(self, context, fileName):
        if(self!=context.getState()):
            context.setState(self)
        data = {
            'LSA':context.getLSA(),
            'LPT': context.getLPT(),
            'MyAlgo':context.getMyAlgo(),
            'BORN_INF_MAX':context.getBornInfMax(),
            'BORN_INF_MOY':context.getBornInfMoy(),
            'RATIOMOY':context.getRatioMoy()
        }
        return self.printAble.toString(data,fileName)

    def doChange(self, context):
        context.setState(self)


class Machine:

    def __init__(self):
        self.taches = []
        self.duree = 0

    def __lt__(self, other):
        return self.duree < other.duree

    def __gt__(self, other):
        return self.duree > other.duree

    def __eq__(self, other):
        return self.duree == other.duree

    def __str__(self):
        return """
        taches : {0}
        durée : {1}
        """.format(self.taches, self.duree)

class Algo(metaclass=ABCMeta):

    def __init__(self):
        super().__init__()
        self.result = None

    @abstractmethod
    def doAlgo(self, m, n, data):
        raise NotImplementedError

    def execute(self, data):
        dataCopy = data
        self.result = []
        if(data['k'] == 1):
            self.result.append(self.doAlgo(dataCopy['m'], dataCopy['n'], dataCopy['data']))
        else:
            for instance in range(0, dataCopy['k']):
                self.result.append(self.doAlgo(dataCopy['m'], dataCopy['n'], dataCopy['data'][instance]))
        #print(self.result)
        return self.result


class LSA(Algo):
    def __init__(self):
        super().__init__()


    def doAlgo(self, m, n, data):
        machines = [Machine() for machine in range(0,m)]
        for index in range(0, n):
            machines.sort()
            machines[0].taches.append(data[index])
            machines[0].duree += data[index]
        machines.sort(reverse=True)
        return machines[0].duree

class LPT(Algo):
    def __init__(self):
        super().__init__()


    def doAlgo(self, m, n, data):
        data.sort(reverse=True)
        machines = [Machine() for machine in range(0,m)]
        for index in range(0, n):
            machines.sort()
            machines[0].taches.append(data[index])
            machines[0].duree += data[index]
        machines.sort(reverse=True)
        return machines[0].duree

class MyAlgo(Algo):

    def __init__(self):
        super().__init__()

    def doAlgo(self, m, n, data):
        """
            - 1) On range les durées des tâches par ordre décrossant
            - 2) On par court les durées et on remplit les machines en commençant par la plus petite,
                 si la durée totale d'une machine plus la durée suivante dépasse la borne moyenne, on passe à la machine suivante, 
                 jusqu'à remplir toutes les machines
            - 3) Pour les durées restantes, on va appliquer le principe du LPT
        """
        data.sort(reverse=True)
        machines = [Machine() for machine in range(0,m)]
        nbSort = 0
        index = 0
        means = sum(data)/m
        while index < n and nbSort < m:
            if(machines[nbSort].duree+data[index] > means):
                nbSort = nbSort + 1
            if(nbSort < m):
                machines[nbSort].taches.append(data[index])
                machines[nbSort].duree += data[index]
            index = index + 1
        for i in range(index-1, n):
            machines.sort()
            machines[0].taches.append(data[i])
            machines[0].duree += data[i]
        machines.sort(reverse=True)
        return machines[0].duree

class Context:

    def __init__(self):
        self.currentState = None
        self.LSA = []
        self.LPT = []
        self.MYALGO = []
        self.BORN_INF_MAX = []
        self.BORN_INF_MOY = []
        self.RATIO_MOY = []


    def getLSA(self):
        las = LSA()
        self.LSA = las.execute(self.currentState.getData())
        return self.LSA

    def getLPT(self):
        lpt = LPT()
        self.LPT = lpt.execute(self.currentState.getData())
        return self.LPT

    def getMyAlgo(self):
        myAlgo = MyAlgo()
        self.MYALGO = myAlgo.execute(self.currentState.getData())
        return self.MYALGO

    def getBornInfMax(self):
        k = self.currentState.getData()['k']
        _data = self.currentState.getData()['data']
        self.BORN_INF_MAX=[]
        if k==1:
            data = _data
            data.sort(reverse=True)
            self.BORN_INF_MAX.append(data[0])
        else:
            for i in range(0,k):
                data = _data[i]
                data.sort(reverse=True)
                self.BORN_INF_MAX.append(data[0])
        return self.BORN_INF_MAX

    def getRatioMoy(self):
        ratioMoyLSA = []
        ratioMoyLPT = []
        ratioMoyMYALGO = []
        size = len(self.BORN_INF_MAX)
        for i in range (0, size):
            M1 = max(self.BORN_INF_MAX[i], self.BORN_INF_MOY[i])
            ratioMoyLSA.append(self.LSA[i]/M1)
            ratioMoyLPT.append(self.LPT[i]/M1)
            ratioMoyMYALGO.append(self.MYALGO[i]/M1)
        self.RATIO_MOY = {
            'LSA':sum(ratioMoyLSA)/size,
            'LPT':sum(ratioMoyLPT)/size,
            'MYALGO':sum(ratioMoyMYALGO)/size
        }
        return self.RATIO_MOY

        return self.BORN_INF_MAX

    def getBornInfMoy(self):
        k = self.currentState.getData()['k']
        m = self.currentState.getData()['m']
        _data = self.currentState.getData()['data']
        self.BORN_INF_MOY = []
        if k==1:
            data = _data
            self.BORN_INF_MOY.append(sum(data)/m)
        else:
            for i in range(0,k):
                data = _data[i]
                self.BORN_INF_MOY.append(sum(data)/m)
        return self.BORN_INF_MOY

    def setState(self, state):
        self.currentState = state

    def getState(self):
        return self.currentState








# Demo

def validChoice(choiceNum,choice):
    return choice in choiceNum

choices=[1,2,3]

menu = """

(1) Choose the mode of instances insertion

(2) See the result

(3) Exit

"""

instancesMenu = """

(If) file

(Ic) keyboard

(Ig) generated

(B) Back

"""

c = Context()
exit=0
choiceInsertion = ""
while (exit!=1):
    print(menu)
    choice = int(input("Select one of this mode "))
    while ((validChoice(choices,choice))==False):
        print("You have made mistake")
        print(menu)
        choice = int(input("Select one of this mode "))


    if (choice == 1):
        print(instancesMenu)
        choiceInsertion = input("Select one of this instances insertion mode ")

        while (choiceInsertion!="If") and (choiceInsertion!="Ic") and (choiceInsertion!="Ig") and (choiceInsertion!="B"):
            print("You have made mistake")
            print(instancesMenu)
            choiceInsertion = input("Select one of this instances insertion mode ")


        if(choiceInsertion=="If"):
            file = input("Insert file name : ")
            s = IfState()
            s.doChange(c)
            c.getState().read(file)

        elif(choiceInsertion=="Ic"):
            s = IcState()
            s.doChange(c)
            c.getState().read()

        elif(choiceInsertion=="Ig"):
            s = IgState()
            s.doChange(c)
            c.getState().read()

    elif choice == 2:
        if(choiceInsertion=="If" or choiceInsertion=="Ic"):
            print(c.getState().toString(c))
        elif(choiceInsertion=="Ig"):
            file = input("Insert file name : ")
            print(c.getState().toString(c, file))

    elif choice == 3:
        exit=1
